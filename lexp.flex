import java_cup.runtime.*;
import ludigame.ast.*;
      
%%
   
%class Lexer
%line
%column
%cup
   

%{   
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }	

%}


/////////////////////////////////////////////////////////////////////////////   

LineTerminator 	= \r|\n|\r\n
WhiteSpace     	= [ \t\f]
Number 		= (0|[1-9][0-9]*)(\.[0-9]+)((e|E)(\+|-)?[0-9]+)?
IntNumber 	= (0|[1-9][0-9]*)
Literal		= [A-Z][a-zA-Z0-9_]*
Hex    		= 0[xX]{1}[0-9A-Fa-f]{4}
Boolean 	= true | false
Var		= [a-z][a-z0-9]*
Function	= (piece|pieces|player|players|place|places|activate|opponents|delete|rows|row|column|columns|pass|draw|victory|defeat)
LudiType	= (Piece|Player|Place)
//LudiPlace	= [A-Z].[1-26].
PlayerName	= [A-Z][a-zA-Z]\w*
Xx		= [Xx]{1}
   
%%
   
<YYINITIAL> {
   
	"+"   		{ return symbol(sym.PLUS);}
        "-"   		{ return symbol(sym.MIN);}
        "*"   		{ return symbol(sym.MULT); }
        "/"   		{ return symbol(sym.DIV); }

        "=="    	{ return symbol(sym.EQUAL);}
        "/="    	{ return symbol(sym.NOT_EQUAL);}
        "<"     	{ return symbol(sym.LESS); }
        ">"     	{ return symbol(sym.GREATER); }
        "<="    	{ return symbol(sym.LESS_EQUAL); }
        ">="    	{ return symbol(sym.GREATER_EQUAL); }

        "!"     	{ return symbol(sym.NOT); }
        "&&"    	{ return symbol(sym.AND); }
        "||"    	{ return symbol(sym.OR); }

        "("     	{ return symbol(sym.LPARENT); }
        ")"     	{ return symbol(sym.RPARENT); }

        "["     	{ return symbol(sym.LBRACK); }
        "]"     	{ return symbol(sym.RBRACK); }
        ","     	{ return symbol(sym.COMMA); }
	"."             { return symbol(sym.POINT); }
        
        "="     	{ return symbol(sym.ASSIGN); }
        ";"     	{ return symbol(sym.SEMICOLON);}
        "{"     	{ return symbol(sym.LBRACE);}
        "}"     	{ return symbol(sym.RBRACE);}
        "if"    	{ return symbol(sym.IF);}
        "then"    	{ return symbol(sym.THEN);}
        "else"  	{ return symbol(sym.ELSE);}
        "while" 	{ return symbol(sym.WHILE);}
	"game" 		{ return symbol(sym.GAME);}
	"players" 	{ return symbol(sym.PLAYERS);}
	"pieces" 	{ return symbol(sym.PIECES);}
	"places"	{ return symbol(sym.PLACES);}
	"start" 	{ return symbol(sym.START);}
	"action" 	{ return symbol(sym.ACTION);}
	"end" 		{ return symbol(sym.END);}
	"Infinity" 	{ return symbol(sym.INFINITY);}
	"NaN"		{ return symbol(sym.NAN);}

	{Xx}		{ return symbol(sym.X);}
        {Number}	{ return symbol(sym.NUMBER, new Double(yytext()));}
        {IntNumber}  	{ return symbol(sym.INTEGER, new Integer(yytext()));}
        {Hex}        	{ return symbol(sym.NUMBER, new Double(Integer.decode(yytext())));} 
        {Boolean}    	{ return symbol(sym.BOOLEAN,new Boolean(yytext()));}
	{LudiType}	{ return symbol(sym.LUDITYPE,Ludi.getType(yytext()));}
	{Function}	{ return symbol(sym.FUNCTION,new String(yytext()));}
	{Literal}	{ return symbol(sym.LITERAL,new String(yytext()));}
        {Var}        	{ return symbol(sym.VAR,new String(yytext()));}
        {PlayerName} 	{ return symbol(sym.PLAYERNAME,new String(yytext()));}

        {LineTerminator} { /* nop */ }
        {WhiteSpace}     { /* nop */ }   
}

[^]     { throw new Error("Illegal character <"+yytext()+">"); }
