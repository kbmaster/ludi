import ludigame.ast.*;
import ludigame.exp.*;
import ludigame.stmt.*;

public class MainTest {

	public static void main(String[] args) {
		String name = "TaTeTi";
		String[] players = {"Xs", "Os"};
		String[] pieces = {};
		
		// Places
		Integer[] place1coord = {1,1};
		LudiPlace place1 = new LudiPlace("A1", place1coord);	
		Integer[] place2coord = {1,2};
		LudiPlace place2 = new LudiPlace("A2", place2coord);	
		Integer[] place3coord = {1,3};
		LudiPlace place3 = new LudiPlace("A3", place3coord);	
		Integer[] place4coord = {2,1};
		LudiPlace place4 = new LudiPlace("B1", place4coord);
		Integer[] place5coord = {2,2};
		LudiPlace place5 = new LudiPlace("B2", place5coord);
		Integer[] place6coord = {2,3};
		LudiPlace place6 = new LudiPlace("B3", place6coord);
		Integer[] place7coord = {3,1};
		LudiPlace place7 = new LudiPlace("C1", place7coord);
		Integer[] place8coord = {3,2};
		LudiPlace place8 = new LudiPlace("C2", place8coord);
		Integer[] place9coord = {3,3};
		LudiPlace place9 = new LudiPlace("C3", place9coord);
		LudiPlace[] places = {place1, place2, place3, place4, place5, place6, place7, place8, place9};
		
		LudiStmt start = null;
		
		// Actions 
		
		// Action - name
		String actionName = "Mark";
		
		// Action - parameters
		LudiActionParam actionParam = new LudiActionParam(Ludi.Type.PLACE, "c");
		LudiActionParam[] actionParams = {actionParam};
		
		// Action - condition 
		LudiVariable variable = new LudiVariable("c");
		LudiExp[] ludiNegationExpArgs = {variable};
		LudiFunctionCall ludiNegationExp = new LudiFunctionCall("piece", ludiNegationExpArgs);
		LudiExp condition = new LudiNegation(ludiNegationExp);
		
		// Action - body		
		LudiVariable variable1 = new LudiVariable("c");
		LudiVariable variable2 = new LudiVariable("ActivePlayer");
		LudiExp[] ludiPieceArgs = {variable1, variable2};
		LudiFunctionCall exp1 = new LudiFunctionCall("Piece", ludiPieceArgs);
		LudiStmtExp stmt1 = new LudiStmtExp(exp1);
		
		LudiFunctionCall exp2 = new LudiFunctionCall("pass", null);
		LudiStmtExp stmt2 = new LudiStmtExp(exp2);
		
		LudiStmt[] blockStmts = {stmt1, stmt2};
		LudiStmt body = new LudiBlock(blockStmts);
		LudiAction actionMark = new LudiAction(actionName, actionParams, condition, body);
		LudiAction[] actions = {actionMark};
				
		// End
		LudiStmt end = null;
		
		// Game
		LudiGameDecl game = new LudiGameDecl(name, players, pieces, places, start, actions, end);
		
		System.out.println(game.print());
	}

}
