JAVA=java
JAVAC=javac
JFLEX=jflex
CUPJAR=./libs/java-cup-11b.jar
PKGPATH=/home/kbmaster/compiladores/devel/oblig/proyecto-2018-java-team-05/proyecto-2018
CUP=$(JAVA) -jar $(CUPJAR) <
CP=.:$(PKGPATH):$(CUPJAR)

Main.class: Lexer.java parser.java *.java ludigame/ast/*.java ludigame/stmt/*.java ludigame/exp/*.java

%.class: %.java
	$(JAVAC) -cp $(CP) $^

Lexer.java: lexp.flex
	$(JFLEX) lexp.flex

parser.java: yexp.cup
	$(CUP) yexp.cup

compile: Lexer.java parser.java *.java ludigame/ast/*.java ludigame/stmt/*.java ludigame/exp/*.java
	$(JAVAC) -encoding 'utf8' -cp $(CP)  $^

interactive: Main.class
	 $(JAVA) -cp $(CP) Main

run: Main.class
	$(JAVA) -cp $(CP) Main in/choose2win.ludi > out/choose2win.js

clean:
	rm -f parser.java Lexer.java sym.java  *~ &&  find . -type f -name '*.class' -delete

test : Main.class
	@ ./runtest.sh

