const LudiGame = require('./LudiGame');

class Choose2Win extends LudiGame
{
	get players()
	{
		return ['Player1', 'Player2'];
	}	actions()
	{
		let results=[];
		results.push({action:'Win'});
		results.push({action:'Lose'});
		results.push({action:'Tie'});
		results.push({action:'Pass'});
		return results;
	}

	next(action)
	{
		let result=this.clone();
		if(action.action==='Win')
		{
			victory();
		}

		else if(action.action==='Lose')
		{
			defeat();
		}

		else if(action.action==='Tie')
		{
			draw();
		}

		else if(action.action==='Pass')
		{
			pass();
		}

		return result;
	}
}

 new Choose2Win().startTurn();
