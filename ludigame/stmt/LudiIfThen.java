package ludigame.stmt;

import ludigame.exp.*;

public class LudiIfThen extends LudiStmt{
	private LudiExp condition;
	private LudiStmt bodyThen;
	private LudiStmt bodyElse;
	
	public LudiIfThen(LudiExp condition, LudiStmt bodyThen, LudiStmt bodyElse)
	{
		this.condition 	= condition;
		this.bodyThen 	= bodyThen;
		this.bodyElse 	= bodyElse;
	}
	
	public LudiIfThen(LudiExp condition, LudiStmt bodyThen)
	{
		this.condition 	= condition;
		this.bodyThen 	= bodyThen;
		this.bodyElse 	= null;
	}
	
	public String print()
	{		
		String text = "if (" + condition.print() + "){\n";
		text += bodyThen.print();
		text += "\n}";
		
		if (bodyElse != null)
		{
			text += "else {\n";
			text += bodyElse.print();
			text += "\n}";
		}
		
		return text;
	}
}
