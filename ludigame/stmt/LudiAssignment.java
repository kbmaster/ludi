package ludigame.stmt;

import ludigame.exp.*;

public class LudiAssignment extends LudiStmt{

	private String var;
	private LudiExp value;
	
	public LudiAssignment(String var, LudiExp value)
	{
		this.var = var;
		this.value = value;
	}
	
	public String print()
	{
		return this.var + "=" + value.print();	
	}
}
