package ludigame.stmt;

import ludigame.exp.*;

public class LudiStmtExp extends LudiStmt{
	private LudiExp exp;
	
	public LudiStmtExp(LudiExp exp)
	{
		this.exp = exp;
	}
	
	public String print()
	{
		return this.exp.print();
	}
}
