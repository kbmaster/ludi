package ludigame.stmt;

public class LudiBlock extends LudiStmt{

	private LudiStmt[] sequence;
	
	public LudiBlock(LudiStmt[] sequence)
	{
		this.sequence = sequence;
	}

	
	public String print()
	{
		String text = "";
		
		for (LudiStmt seq : sequence)
		{
			text += "\t\t\t" + seq.print() + ";\n";
		}
		return text;
	}
}
