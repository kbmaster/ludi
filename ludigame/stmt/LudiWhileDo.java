package ludigame.stmt;

import ludigame.exp.*;

public class LudiWhileDo extends LudiStmt {
	private LudiExp condition;
	private LudiStmt body;
	
	public LudiWhileDo(LudiExp condition, LudiStmt body)
	{
		this.condition = condition;
		this.body = body;
	}
	
	public String print()
	{
		String text = "while (";
		text += condition.print() + "){\n";
		text += body.print();
		text += "}\n";
		
		return text;
	}
}
