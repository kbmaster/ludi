package ludigame.ast;

public class Ludi {	
	public enum Type{
	    PIECE,
	    PLAYER,
	    PLACE;
	}
	
	public enum Methods{
		
	}

	public static Ludi.Type  getType(String type)
	{
		switch(type)
		{
			case "Piece": return Ludi.Type.PIECE; 
			case "Place": return Ludi.Type.PLACE; 
			case "Player":return Ludi.Type.PLAYER; 
		}
		
		return null;
	}
}
