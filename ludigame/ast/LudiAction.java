package ludigame.ast;

import ludigame.stmt.*;
import ludigame.exp.*;

public class LudiAction {

	private String name;
	private LudiActionParam[] params;
	private LudiExp condition;
	private LudiStmt body;
	
	public LudiAction(String name, LudiActionParam[] params, LudiExp condition, LudiStmt body)
	{
		this.name = name;
		this.params = params;
		this.condition = condition;
		this.body = body;
	}
	
	public String print()
	{
		String text = "{action:" + "'"+this.name.trim()+"'";
		
		if (this.params != null)
		{
			if (this.params.length > 0)
			{
				text += ", args : [";
				
				if (this.params.length == 1)
				{
					text += this.params[0].print();
				}
				else	
				{
					for (int i = 0; i < this.params.length - 1; i++)
					{
						text += this.params[i].print() + ", ";
					}
					text += this.params[this.params.length-1].print();
				}
				
				text += "]";
			}
		}
		
		text += "}";
		return text;
	}
	
	public boolean hasCondition()
	{
		if (this.condition != null)
		{
			return true;
		}
		return false;
	}
	
	public boolean hasBody()
	{
		if (this.body != null)
		{
			return true;
		}
		return false;
	}
	
	public boolean hasArgs()
	{
		if (this.params!= null)
		{
			if (this.params.length > 0)
			{
				return true;
			}
		}
		return false;
	}
	
	public LudiExp getCondition()
	{
		return this.condition;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String printDeclaredArgs()
	{
		String declaredArgsText = "";		
		int index = 0;
		
		for (LudiActionParam param : params)
		{
			declaredArgsText += "\t\t\tlet " + param.print() + " = action.args[" + String.valueOf(index) + "];\n";
			index++;
		}			
		return declaredArgsText;
	}
	
	public String printBody()
	{
		return this.body.print();
	}
}
