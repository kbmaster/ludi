package ludigame.ast;

public class LudiActionParam {

	private Ludi.Type type;
	private String name;
	
	public LudiActionParam(Ludi.Type type, String name)
	{
		this.type = type;
		this.name = name;
	}
	
	public String print()
	{
		return this.name;
	}
}


