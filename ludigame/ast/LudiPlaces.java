package ludigame.ast;

public final class LudiPlaces {

	private static Integer count=0; 

	public static LudiPlace place(String Place)
	{
		count++;
		Integer [] coord ={1,count};
		return new LudiPlace(Place, coord);
	}

	public static LudiPlace[] rango(String a,String b)
    {
		if (a.length() > 0 && b.length() > 0)
		{
			String alphabet 		= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			char[] alphabetArray 	= alphabet.toCharArray();
			char letterPlaceA 		= a.charAt(0);
			char letterPlaceB 		= b.charAt(0);		
			int numberPlaceA 		= Integer.valueOf(a.substring(1));
			int numberPlaceB 		= Integer.valueOf(b.substring(1));
			
			int[] coordA = {alphabet.indexOf(letterPlaceA)+1, numberPlaceA};
			int[] coordB = {alphabet.indexOf(letterPlaceB)+1, numberPlaceB}; 
						
			int size = 0;		
			// Manera asquerosa de calcular el size
			for (int x=coordA[0]; x<=coordB[0]; x++)
			{	
				for (int y=coordA[1]; y<=coordB[1]; y++)
				{
					size++;
				}
			}
			
		 	LudiPlace[] damero 	= new LudiPlace[size];
			
			int index = 0;
			for (int x=coordA[0]; x<=coordB[0]; x++)
			{	
				for (int y=coordA[1]; y<=coordB[1]; y++)
				{		
					String id	 	= alphabetArray[x-1]+String.valueOf(y);
					Integer[] coord = {x,y};		
					damero[index] 	= new LudiPlace(id, coord);
					index++;
				}
			}	
			return damero;
		}	
		return null;
    }

	 public static LudiPlace[] damero(int a,int b)
    {
		if (a>0 && b>0)
		{
		 	char[] alphabet 	= "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();		 
		 	Integer size 		= a * b;
		 	LudiPlace[] damero 	= new LudiPlace[size];
			
			int index = 0;
			for (int x=1; x<=a; x++)
			{	
				for (int y=1; y<=b; y++)
				{		
					String id	 	= alphabet[x-1]+String.valueOf(y);
					Integer[] coord = {x,y};		
					damero[index] 	= new LudiPlace(id, coord);
					index++;
				}
			}	
			return damero;
		}		
		return null;
    }

	
}
