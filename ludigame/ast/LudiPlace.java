package ludigame.ast;

public class LudiPlace {
	private String id;
	private Integer[] coord;
	
	public LudiPlace(String id, Integer[]coord)
	{
		this.id = id;
		this.coord = coord;
	}
	
	public String print()
	{
		return "this.PLACES." + id;
	}	
}

