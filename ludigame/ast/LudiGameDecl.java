package ludigame.ast;

import ludigame.stmt.*;

public class LudiGameDecl extends LudiGame{
	
	private String name;
	private String[] players;
	private String[] pieces;
	private LudiPlace[] places;
	private LudiStmt start;
	private LudiAction[] actions;
	private LudiStmt end;

	public LudiGameDecl(String name, String[] players, String[] pieces, LudiPlace[] places, LudiStmt start, LudiAction[] actions, LudiStmt end)
	{
		this.name = name;
		this.players = players;
		this.pieces = pieces;
		this.places = places;
		this.start = start;
		this.actions = actions;
		this.end = end;
	}
	
	public String print()
	{
		String text = "const LudiGame = require('./LudiGame');\n\n";
		text += "class " + this.name.trim() + " extends LudiGame\n{";

		// get players()
		if (this.players != null)
		{
			text += printPlayers();
		}
		
		// get pieces()
		if (this.pieces != null)
		{
			text += printPieces();
		}
		
		// get places()
		if (this.places != null)
		{
			text += printPlaces();
		}		
		
		// startState()			
		if (start != null)
		{
			text += printStartState();
		}		
		
		// actions()
		if (this.actions != null)
		{
			text += printActions();
		
			// next(action)
			text += "\n\tnext(action)\n\t{\n";
			text += "\t\tlet result=this.clone();\n";
			
			int count = 1;
			for (LudiAction action : actions)
			{
				if (count==1)
				{
					text+="\t\tif(action.action==='" ;
				} else
				{
					text+="\t\telse if(action.action==='" ;
				}
				
				text += action.getName()+"')\n\t\t{\n";
				
				// Action Args
				if (action.hasArgs())
				{
					text += action.printDeclaredArgs();
				}
				
				// Action Condition
				if (action.hasCondition())
				{
					text += "";
				}
				
				// Action Body
				if (action.hasBody())
				{
					text += action.printBody();
				}
				
				text += "\t\t}\n\n";
				
				count=0;
			}
			text += "\t\treturn result;\n";
			text += "\t}\n";
		}
		
		// end()	
		if (end != null)
		{
			text += printEnd();	
		}
		
		// printTable()				
		
		text += "}";

		text+="\n\n new "+this.name+"().startTurn();";
		
		return text;
	}
	
	
	public String printPlayers()
	{
		String playersText = "";
	
		if (this.players.length > 0)
		{
			playersText += "\n";
			playersText += "\tget players()\n";
			playersText += "\t{\n";
			playersText += "\t\treturn [";
			
			if (this.players.length == 1)
			{
				playersText += "'"+players[0].trim()+"'";
			}
			else	
			{
				for (int i = 0; i < players.length - 1; i++)
				{
					playersText += "'"+players[i].trim()+"'" + ", ";
				}
				playersText += "'"+players[players.length-1].trim()+"'";
			}
			
			playersText += "];\n\t}";
		}
		return playersText;
	}
	
	public String printPieces()
	{
		String piecesText = "";
		
		if (this.pieces.length > 0)
		{	
			piecesText += "\n";
			piecesText += "get pieces()\n";
			piecesText += "{\n";
			piecesText += "\treturn [";
			
			if (this.pieces.length == 1)
			{
				piecesText += "this.PIECES." + this.pieces[0].trim();
			}
			else	
			{
				for (int i = 0; i < this.pieces.length - 1; i++)
				{
					piecesText += "this.PIECES." + this.pieces[i].trim() + ", ";
				}
				piecesText += "this.PIECES." + this.pieces[this.pieces.length-1].trim();
			}		
			piecesText += "];\n\t}";
		}
		return piecesText;
	}
	
	public String printActions()
	{
		String actionsText = "";
		
		actionsText += "\tactions()\n";
		actionsText += "\t{\n";
		actionsText += "\t\tlet results=[];\n";
		
		for (LudiAction action : this.actions)
		{
			if (action.hasCondition())
			{
				actionsText += "\t\tif (" + action.getCondition().print() +"){\n\t";
			}
			
			actionsText += "\t\tresults.push("+action.print()+");\n";
			
			if (action.hasCondition())
			{
				actionsText += "\t\t}\n";
			}
		}
		actionsText += "\t\treturn results;\n\t}\n";
		
		return actionsText;
	}
	
	public String printPlaces()
	{
		String placesText = "";
		
		if (this.places.length > 0)
		{	
			placesText += "\n";
			placesText += "\tget places()\n";
			placesText += "\t{\n";
			placesText += "\t\treturn [";
			
			if (this.places.length == 1)
			{
				placesText += this.places[0].print();
			}
			else	
			{			
				for (int i = 0; i < this.places.length - 1; i++)
				{
					placesText += this.places[i].print() + ", ";
				}
				placesText += this.places[this.places.length-1].print();
			}		
			placesText += "];\n\t}\n";
		}
		return placesText;		
	}
	
	public String printStartState()
	{
		String startStateText = "";	
		startStateText += "\tstartState()\n\t{\n\t\t[";
		startStateText += this.start.print();
		startStateText += "];\n\t}\n";			
		return startStateText;
	}
	
	public String printEnd()
	{
		String endText = "";
		endText += "end()\n";
		endText += "{\n\t";
		endText += this.end.print();
		endText += "}\n";	
		return endText;
	}
}
