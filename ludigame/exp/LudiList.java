package ludigame.exp;

public class LudiList extends LudiExp{
	private LudiExp[] values;
	
	public LudiList(LudiExp[] values)
	{
		this.values = values;
	}
	
	public String print()
	{
		String text = "[";
		
		if (this.values.length >0)
		{
			text += printValues();
		}
		
		text += "]";
		return text;
	}
	
	public String printValues()
	{
		String text = "";
		
		if (this.values.length == 1)
		{
			text += this.values[0].print();
		}
		else	
		{
			for (int i = 0; i < this.values.length - 1; i++)
			{
				text += this.values[i].print() + ", ";
			}
			text += this.values[this.values.length-1].print();
		}
		
		return text;
	}
}
