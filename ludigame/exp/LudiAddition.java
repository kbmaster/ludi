package ludigame.exp;

public class LudiAddition extends LudiNumericExp{
	private LudiExp left;
	private LudiExp right;
	
	public LudiAddition(LudiExp left, LudiExp right)
	{
		this.left = left;
		this.right = right;
	}

	public String print()
	{
		return this.left.print() + " + " + this.right.print();
	}
}
