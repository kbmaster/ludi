package ludigame.exp;

public class LudiFunctionCall extends LudiExp{
	private String funId;
	private LudiExp[] args;
	
	public LudiFunctionCall(String funId, LudiExp[] args)
	{
		this.funId = funId;
		this.args = args;
	}
	
	public String print()
	{
		String text = this.funId;
		text += "(";
		
		if (args != null)
		{
			text += printArgs();
		}
		
		text += ")";
		
		return text;
	}
	
	public String printArgs()
	{
		String text = "";
		if (this.args.length == 1)
		{
			text += this.args[0].print();
		}
		else	
		{
			for (int i = 0; i < this.args.length - 1; i++)
			{
				text += this.args[i].print() + ", ";
			}
			text += this.args[this.args.length-1].print();
		}
		return text;
	}
}
