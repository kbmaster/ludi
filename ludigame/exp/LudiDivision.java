package ludigame.exp;

public class LudiDivision extends LudiNumericExp{
	private LudiExp left;
	private LudiExp right;
	
	public LudiDivision(LudiExp left, LudiExp right)
	{
		this.left = left;
		this.right = right;
	}
	
	public String print()
	{
		return this.left.print() + " / " + this.right.print();
	}
}
