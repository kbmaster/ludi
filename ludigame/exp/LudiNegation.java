package ludigame.exp;

public class LudiNegation extends LudiExp{
	private LudiExp right;
	
	public LudiNegation(LudiExp right)
	{
		this.right = right;
	}
	
	public String print()
	{
		return "!" + this.right.print();
	}
}
