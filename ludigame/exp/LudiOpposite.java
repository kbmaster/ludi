package ludigame.exp;

public class LudiOpposite extends LudiExp{
	private LudiExp right;
	
	public LudiOpposite(LudiExp right)
	{
		this.right = right;
	}
	
	public String print()
	{
		return "-" + this.right.print();
	}
}
