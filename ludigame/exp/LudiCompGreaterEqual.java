package ludigame.exp;

public class LudiCompGreaterEqual extends LudiBooleanExp{
	private LudiExp left;
	private LudiExp right;
	
	public LudiCompGreaterEqual(LudiExp left, LudiExp right)
	{
		this.left = left;
		this.right = right;
	}
	
	public String print()
	{
		return this.left.print() + " >= " + this.right.print();
	}
}
