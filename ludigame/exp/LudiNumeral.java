package ludigame.exp;

public class LudiNumeral extends LudiExp{
	private double value;
	
	public LudiNumeral(double value)
	{
		this.value = value;
	}
	
	public String print()
	{
		return String.valueOf(this.value);
	}
}
