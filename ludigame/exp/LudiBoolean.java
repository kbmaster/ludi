package ludigame.exp;

public class LudiBoolean extends LudiExp{
	private boolean value;
	
	public LudiBoolean(boolean value)
	{
		this.value = value;
	}
	
	public String print()
	{
		return String.valueOf(this.value);
	}
}
