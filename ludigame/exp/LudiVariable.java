package ludigame.exp;

public class LudiVariable extends LudiExp{
	private String var;
	
	public LudiVariable(String var)
	{
		this.var = var;
	}
	
	public String print()
	{
		return this.var;
	}
}
