package ludigame.exp;

public class LudiSubstraction extends LudiNumericExp{
	private LudiExp left;
	private LudiExp right;
	
	public LudiSubstraction(LudiExp left, LudiExp right)
	{
		this.left = left;
		this.right = right;
	}
	
	public String print()
	{
		return this.left.print() + " - " + this.right.print();
	}
}
