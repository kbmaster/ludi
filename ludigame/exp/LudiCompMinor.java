package ludigame.exp;

public class LudiCompMinor extends LudiBooleanExp{
	private LudiExp left;
	private LudiExp right;
	
	public LudiCompMinor(LudiExp left, LudiExp right)
	{
		this.left = left;
		this.right = right;
	}
	
	public String print()
	{
		return this.left.print() + " < " + this.right.print();
	}
}


