const LudiGame = require('./LudiGame');

class TaTeTi  extends LudiGame
{
        get players()
        {
                return ['Xs','Os'];
        }



	startState()
	{
		return [];
	}



	get places()
	{
		return[
			this.PLACES.A1,this.PLACES.A2,this.PLACES.A3,
			this.PLACES.B1,this.PLACES.B2,this.PLACES.B3,
			this.PLACES.C1,this.PLACES.C2,this.PLACES.C3
		      ];
	}



        actions()
        {
                let results=[];
			for(var c of this.places)
				if(!this.piece(c)){
					results.push({ action:'Mark', args: [c] });}
		 return results;
        }



        next(action)
        {
		let result=this.clone();
		
                if(action.action==="Mark")
		{
			let c = action.args[0];

			if(!this.piece(c)) 
			{
				result.newPiece(this.PIECE,this.activePlayer, c);
				result.pass();
			}
			else
			{
				throw new Error("Invalid action "+ JSON.stringify(action) +"!");
			}

		}
                else
                if(action.action==="Pass")result.pass();

                return result;
        }




	end()
	{
		let p = this.player(this.PLACES.A1,this.PLACES.A2,this.PLACES.A3) ||
		this.player(this.PLACES.B1,this.PLACES.B2,this.PLACES.B3) ||
		this.player(this.PLACES.C1,this.PLACES.C2,this.PLACES.C3) ||
		this.player(this.PLACES.A1,this.PLACES.B1,this.PLACES.C1) ||
		this.player(this.PLACES.A2,this.PLACES.B2,this.PLACES.C2) ||
		this.player(this.PLACES.A3,this.PLACES.B3,this.PLACES.C3) ||
		this.player(this.PLACES.A1,this.PLACES.B2,this.PLACES.C3) ||
		this.player(this.PLACES.A3,this.PLACES.B2,this.PLACES.C1);

		if(p) {this.victory(p);}

            p =	this.player(this.PLACES.A1) && this.player(this.PLACES.A2) &&
		this.player(this.PLACES.A3) && this.player(this.PLACES.B1) &&
		this.player(this.PLACES.B2) && this.player(this.PLACES.B3) &&
		this.player(this.PLACES.C1) && this.player(this.PLACES.C2) &&
		this.player(this.PLACES.C3); 

		if(p) {this.draw();}

	}


	printActions(actions)
        {
                console.log("Juega: "+this.ActivePlayer);
                actions.forEach(function(action,i)
                {
                        console.log((i+1)+"-"+action.action+"("+action.args[0].place+")");
                });
        }


	printTable()
	{
		for(var i=0;i<3;i++)
		{
			console.log((this.player(this.places[3*i])||"  ")+"|"+(this.player(this.places[3*i+1])||"  ")+"|"+(this.player(this.places[3*i+2])||"  "));
		}
	}


}


new TaTeTi("Tateti").startTurn();
