const readline = require('readline')

//interseccion de arays de piezas
Array.prototype.intersect=function(b)
{
	return this.filter(function(o)
	{	
		if(o.coord)
			return -1 !== b.map(function(e){return e.place}).indexOf(o.place);	
		else if(o.place)
			return -1 !== b.map(function(e){return e.place.place}).indexOf(o.place.place);	
		else 
			return -1 !== b.indexOf(o);
	});
}

//union de arrays de piezas
Array.prototype.union=function(b)
{
	return this.concat(b).filter(function(item, pos, self){	return self.map(function(e){return e.place.place;}).indexOf(item.place.place) == pos;});
}


class LudiGame 
{
        constructor(gameName)
        {
		this.name=gameName;
		this.PLAYER='Player';
		this.PIECE='Piece';
		this.PLACE={place:'Place',coord:[1,1]};
		this.PLACES={};

		for(var i=65;i<91;i++)
		{
			for(var j=1;j<27;j++)
			{
				let key=String.fromCharCode(i)+j;
				this.PLACES[key]={place:key,coord:[i-64,j]};
			}
		}
		
		this.PLACE={place:'Place',coord:[1,1]};
                this.ActivePlayer=this.players[0];
		this.state=this.startState();
                this.endgame=false;

        }


	//aux
	printActions(actions)
	{
		console.log("Juega: "+this.ActivePlayer);
		actions.forEach(function(action,i)
		{
			console.log((i+1)+"-"+action.action);
		});
	}

	
	startTurn()
        {
                this.end();
		if(this.endgame)return;

		var reader = readline.createInterface({ input: process.stdin, output: process.stdout});
                this.printTable();
		this.printActions(this.actions());

                reader.on("line",function(choose)
                {
                        let actions=this.actions();
                        let next=this.next(actions[choose-1]);
                        reader.close();
                        next.startTurn();

                }.bind(this));

                reader.setPrompt("");
                reader.prompt();
        }


	////////////////////////////////////////////////////////////////////////
	player()
	{
                let result=this._players.apply(this,arguments);
                return (result && result.length==1)? result[0] : null;	
	}
	
	_players()
	{
		var intersect=[];

                for(var a in arguments)
                {
                        var cond = arguments[a];

                        if(Array.isArray(cond))
                        {
                                var union=[];
                                cond.forEach(function(elem)
                                {
                                        let p =this._pieces_(elem);
                                        union=union.union(p);

                                }.bind(this));

                                intersect.push(union.map(function(o){return o.owner;}).filter(function(item, pos, self){ return self.indexOf(item) == pos;}));

                        }else
                        {
                                intersect.push(this._pieces_(cond).map(function(o){return o.owner;}));
                        }

                }

                var result=intersect[0];
                intersect.splice(0,1);

                intersect.forEach(function(e)
                {
                        result=result.intersect(e);
                });

                return (result.length)? result : null;		

	}

        place()
        {
                let result=this._places.apply(this,arguments);
                return (result && result.length==1)? result[0] : null;
        }

        _places()
        {
                
		var intersect=[];

                for(var a in arguments)
                {
                        var cond = arguments[a];

                        if(Array.isArray(cond))
                        {
                                var union=[];
                                cond.forEach(function(elem)
                                {
                                        let p =this._pieces_(elem);
                                        union=union.union(p);

                                }.bind(this));

				//console.log(union.map(function(o){return o.place;}));

				intersect.push(union.map(function(o){return o.place;}));

                        }else
                        {
                                intersect.push(this._pieces_(cond).map(function(o){return o.place;}));
                        }

                }

                var result=intersect[0];
                intersect.splice(0,1);

                intersect.forEach(function(e)
                {
                        result=result.intersect(e);
                });

                return (result.length)? result : null;
        }	
	
	//////////////////////////////////////////////////////////////////
	
	newPiece(piece,owner,place)
	{
		 this.state[this.state.length]={piece:piece,owner:owner,place:place};
	}

	piece()
	{
		let result=this._pieces.apply(this,arguments);
		return (result && result.length==1)? result[0] : null;		
	}

	
	//_pieces(C1,C2,C3)=_pieces(C1) and _pieces(C2) and _pieces(C2)
	//_pieces([C1,C2,C3])=_pieces(C1) or _pieces(C2) or _pieces(C2)
	_pieces()
	{
		var intersect=[];		

		for(var a in arguments)
		{
			var cond = arguments[a];
			
			if(Array.isArray(cond))
			{
				var union=[];
				cond.forEach(function(elem)
				{
					let p =this._pieces_(elem);
					union=union.union(p);
			
				}.bind(this));
				
				intersect.push(union);

			}else
			{
				intersect.push(this._pieces_(cond));
			}

		}

		var result=intersect[0];
		intersect.splice(0,1);	

		intersect.forEach(function(e)
		{
			result=result.intersect(e);
		});

		return (result.length)? result : null;

	}

	//retorna el resultado de aplicar la condicon al set de piezas
	_pieces_(cond)
	{		
		if(typeof cond ==='string')
                {
			if(this.players.includes(cond))
				return  this.state.filter(function(o){ return o.owner==cond});
                        else  if(this.pieces.includes(cond))
                        	return this.state.filter(function(o){ return o.piece==cond});
		}else if(typeof cond ==='object' && cond.place)  return this.state.filter(function(o){return o.place.place==cond.place});
		
		return [];
	}


	delete(pieces)	
	{
		pieces=(Array.isArray(pieces))? pieces:[pieces];

		pieces.forEach(function(p)
		{
			this.state.forEach(function(piece,i)
			{
				if(p.place.place===piece.place.place)
					this.state.splice(i,1);
			}.bind(this));	
		
		}.bind(this));
	}
	
	move(piece,place)
	{
		if(pieces(place))throw new Error("Destino ocupado");
		let p=this.piece(piece);
		p.place=place;
	}

	row(args)
	{
		p=piece(args);
		if(p)return p.place.coord[0];
		else throw new Error("No existe la pieza");		
	}

	column(args)
	{
		 p=piece(args);
                 if(p)return p.place.coord[1];
		 else throw new Error("No existe la pieza");	
	}


	////////////////////////////////////////////////////

	clone()
	{
		let clone = Object.assign(Object.create(Object.getPrototypeOf(this)),this);
		return clone;
	}

	pass()
	{
		let i=this.players.indexOf(this.ActivePlayer);
		i=(this.players.length-1==i)?0:i+1;
		this.activate(this.players[i]);
	}

	draw()
	{
		console.log("Juego finalizado:Empate");
		this.endgame=true;
	}

	victory(player)
	{
		if(!player)player=this.ActivePlayer;
		this.endgame=true;
		console.log("Juego finalizado:Gana "+player);
	}

	defeat(player)
	{
		if(!player)player=this.ActivePlayer;
		this.endgame=true
		console.log("Juego finalizado:Piede "+player);
	}

	activate(player)
	{
		this.ActivePlayer=player;
	}

	get activePlayer()
	{
		return this.ActivePlayer;
	}

        get pieces(){return [this.PIECE];}	//@override   lista de piezas definidas  
	get places() { return [this.PLACE];} 	//@override   lista de lugares definidos	
        get players() { return [this.PLAYER];}  //@override   listado de jugadores definidos
	startState(){ return []; } 		//@override   lista de pieza,place,jugador
	actions(){ return []; }			//@override   lsita de condition actions 
	next(action,update=false){}		//@override   lista de acciones por nombre	
	end(){}					//@override   condiciones de finalizacion
	printTable(){}			//@override   muestra tablero

}

module.exports=LudiGame;


