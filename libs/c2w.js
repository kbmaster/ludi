const LudiGame = require('./LudiGame');

class ChooseToWin  extends LudiGame
{
	get players()
        {
                return ['P1','P2'];
        }

        actions()
        {
                let results=[];
                results.push({action:'Win'});
                results.push({action:'Lose'});
                results.push({action:'Pass'});
                return results;
        }

        next(action)
        {
                let result=this.clone();

                if(action.action==="Win")result.victory();
                else
                if(action.action==="Lose")result.defeat();
                else
                if(action.action==="Pass")result.pass();

                return result;
        }

}


ctw=new ChooseToWin("c2w");
ctw.startTurn();
